package com.meridianid.farizdotid.belajarloginsosmed.instagram.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.meridianid.farizdotid.belajarloginsosmed.R;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.Datum;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.Images;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.ResultResponseMedia;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class InstagramPictureAdapter extends RecyclerView.Adapter<InstagramPictureAdapter.MyViewHolder> {

    Context mContext;
    List<Datum> datumList;


    public InstagramPictureAdapter(){

    }

    public InstagramPictureAdapter(List<Datum> listData, Context context){
        this.datumList = listData;
        this.mContext = context;
    }

    @Override
    public InstagramPictureAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_picture,
                parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InstagramPictureAdapter.MyViewHolder holder, int position) {
        Datum datum = datumList.get(position);
        Glide.with(mContext)
                .load(datum.getImages().getLowResolution().getUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivResultPicture);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivResultPicture)
        ImageView ivResultPicture;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
