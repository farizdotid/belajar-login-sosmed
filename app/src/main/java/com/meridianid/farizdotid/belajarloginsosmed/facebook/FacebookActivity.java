package com.meridianid.farizdotid.belajarloginsosmed.facebook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.meridianid.farizdotid.belajarloginsosmed.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacebookActivity extends AppCompatActivity {

    @BindView(R.id.btnLogin)
    LoginButton btnLogin;
    @BindView(R.id.tvResult)
    TextView tvResult;

    private CallbackManager callbackManager;
    private ProfileTracker profilTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);

        ButterKnife.bind(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        btnLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("debug", "onSuccess: ");
                tvResult.setText(loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                Log.i("debug", "onCancel: ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("debug", "onError: ERROR > " + error.getMessage());
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profilTracker.stopTracking();
    }
}
