package com.meridianid.farizdotid.belajarloginsosmed.util.api;

import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.ResultResponseMedia;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public interface BaseApiService {

//    @GET("v1/users/{userid}/media/recent/?access_token=")
//    Call<List<ResultResponseMedia>> getPicture(@Path("userid") String userId,
//                                               @Query("access_token") String accessToken);

    @GET("v1/users/{userid}/media/recent/?access_token=")
    Call<ResultResponseMedia> getPicture(@Path("userid") String userId,
                                               @Query("access_token") String accessToken);
}
