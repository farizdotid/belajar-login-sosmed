package com.meridianid.farizdotid.belajarloginsosmed.instagram;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class InstagramSession {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    public static final String CLIENT_ID = "5a60fe608e4e40b2b45204a3f5b7c256";
    public static final String CLIENT_SECRET = "e690b6a05e0b4af6ad63cec0739815f0";
    public static final String CALLBACK_URL = "https://farizdotid.com/";

    private static final String SHARED = "Instagram_Preferences";
    private static final String API_USERNAME = "username";
    private static final String API_ID = "id";
    private static final String API_FULLNAME = "name";
    private static final String API_ACCESS_TOKEN = "access_token";
    private static final String API_USER_IMAGE = "user_image";

    public InstagramSession(Context context) {
        sharedPref = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void storeAccessToken(String accessToken, String id,
                                 String username, String urlImage, String fullName) {
        editor.putString(API_ACCESS_TOKEN, accessToken);
        editor.putString(API_ID, id);
        editor.putString(API_USERNAME, username);
        editor.putString(API_USER_IMAGE, urlImage);
        editor.putString(API_FULLNAME, fullName);
        editor.commit();
    }

    public void storeAccessToken(String accessToken) {
        editor.putString(API_ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    /**
     * Reset access token and user name
     */
    public void resetAccessToken() {
        editor.putString(API_ACCESS_TOKEN, null);
        editor.putString(API_ID, null);
        editor.putString(API_USERNAME, null);
        editor.putString(API_USER_IMAGE, null);
        editor.putString(API_FULLNAME, null);
        editor.commit();
    }

    /**
     * Get user name
     *
     * @return User name
     */
    public String getUsername() {
        return sharedPref.getString(API_USERNAME, null);
    }

    /**
     *
     * @return
     */
    public String getId() {
        return sharedPref.getString(API_ID, null);
    }

    /**
     *
     * @return
     */
    public String getFullName() {
        return sharedPref.getString(API_FULLNAME, null);
    }

    /**
     * Get access token
     *
     * @return Access token
     */
    public String getAccessToken() {
        return sharedPref.getString(API_ACCESS_TOKEN, null);
    }

    /**
     * Get userImage
     *
     * @return userImage
     */
    public String getUserImage() {
        return sharedPref.getString(API_USER_IMAGE, null);
    }
}
