package com.meridianid.farizdotid.belajarloginsosmed.util.api;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class UtilsApi {

    public static final String BASE_URL_API_INSTAGRAM = "https://api.instagram.com/";

    public static BaseApiService getAPIServiceInstagram(){
        return RetrofitClient.getClient(BASE_URL_API_INSTAGRAM).create(BaseApiService.class);
    }

}
