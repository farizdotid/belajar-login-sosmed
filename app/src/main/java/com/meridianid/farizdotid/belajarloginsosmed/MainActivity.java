package com.meridianid.farizdotid.belajarloginsosmed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.meridianid.farizdotid.belajarloginsosmed.facebook.FacebookActivity;
import com.meridianid.farizdotid.belajarloginsosmed.googleplus.GooglePlusActivity;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.InstagramActivity;
import com.meridianid.farizdotid.belajarloginsosmed.twitter.TwitterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "bmqMBis0IRqrG400tMqABjlHJ";
    private static final String TWITTER_SECRET = "deF4LEJloPe8EEafGBGuQuRpXLPnHVoQvdMan9LLmRSIgVG62a";

    @BindView(R.id.btnInstagram)
    Button btnInstagram;
    @BindView(R.id.btnFacebook)
    Button btnFacebook;
    @BindView(R.id.btnTwitter)
    Button btnTwitter;
    @BindView(R.id.btnGooglePlus)
    Button btnGooglePlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        btnInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity(InstagramActivity.class);
            }
        });

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity(FacebookActivity.class);
            }
        });

        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity(TwitterActivity.class);
            }
        });

        btnGooglePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity(GooglePlusActivity.class);
            }
        });
    }

    private void callActivity(Class classDestination){
        startActivity(new Intent(MainActivity.this, classDestination));
    }
}
