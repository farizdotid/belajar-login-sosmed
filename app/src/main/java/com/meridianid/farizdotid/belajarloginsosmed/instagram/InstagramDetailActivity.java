package com.meridianid.farizdotid.belajarloginsosmed.instagram;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.meridianid.farizdotid.belajarloginsosmed.R;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.adapter.InstagramPictureAdapter;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.Datum;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.Images;
import com.meridianid.farizdotid.belajarloginsosmed.instagram.model.ResultResponseMedia;
import com.meridianid.farizdotid.belajarloginsosmed.util.api.BaseApiService;
import com.meridianid.farizdotid.belajarloginsosmed.util.api.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstagramDetailActivity extends AppCompatActivity {

    @BindView(R.id.ivResultPhoto)
    ImageView ivResultPhoto;
    @BindView(R.id.tvResultFullName)
    TextView tvResultFullName;
    @BindView(R.id.rvResultGambar)
    RecyclerView rvResultGambar;
    ProgressDialog loading;

    private InstagramApp instagramApp;
    BaseApiService mApiService;

    Context mContext;
    InstagramPictureAdapter mAdapter;
    List<Datum> datumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram_detail);

        ButterKnife.bind(this);
        mContext = this;
        instagramApp = new InstagramApp(this, InstagramSession.CLIENT_ID, InstagramSession.CLIENT_SECRET,
                InstagramSession.CALLBACK_URL);
        mApiService = UtilsApi.getAPIServiceInstagram();

        Glide.with(this).load(instagramApp.getProfileImage()).into(ivResultPhoto);
        tvResultFullName.setText(instagramApp.getFullName());

        initRecyclerView();

        loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
        getDataPicture();
    }

    private void initRecyclerView(){
        mAdapter = new InstagramPictureAdapter(datumList, mContext);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        rvResultGambar.setLayoutManager(mLayoutManager);
        rvResultGambar.setItemAnimator(new DefaultItemAnimator());
    }

    private void getDataPicture(){
        mApiService.getPicture(instagramApp.getId(), instagramApp.getAccessToken()).enqueue(
                new Callback<ResultResponseMedia>() {
                    @Override
                    public void onResponse(Call<ResultResponseMedia> call, Response<ResultResponseMedia> response) {
                        Log.i("debug", "onResponse: ");
                        if (response.isSuccessful()){
                            Log.i("debug", "onResponse: BERHASIL");

                            loading.dismiss();
                            List<Datum> listData = response.body().getData();
                            Log.i("debug", "onResponse: TOTAL LIKE > " + listData.get(0).getLikes().getCount());
                            Log.i("debug", "onResponse: TOTAL DATA > " + listData.size());

                            rvResultGambar.setAdapter(new InstagramPictureAdapter(listData, mContext));
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Log.i("debug", "onResponse: GA BERHASIL");
                            loading.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResultResponseMedia> call, Throwable t) {
                        loading.dismiss();
                        Log.e("debug", "onFailure: ERROR > "+ t.getMessage());
                    }
                }
        );
    }

}
