package com.meridianid.farizdotid.belajarloginsosmed.instagram;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.meridianid.farizdotid.belajarloginsosmed.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InstagramActivity extends AppCompatActivity {

    @BindView(R.id.btnLogin)
    Button btnLogin;

    private InstagramApp instagramApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram);

        ButterKnife.bind(this);

        instagramApp = new InstagramApp(this, InstagramSession.CLIENT_ID, InstagramSession.CLIENT_SECRET,
                InstagramSession.CALLBACK_URL);

        instagramApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(InstagramActivity.this, "Halo " + instagramApp.getUserName(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(InstagramActivity.this, InstagramDetailActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFail(String error) {
                Log.e("debug", "onFail: ERROR > " + error);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instagramApp.authorize();
            }
        });
    }

}
